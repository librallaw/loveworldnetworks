<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Event;
use App\Information;
use App\Resource;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{
    public function loaddashboard(){



    	$data['events'] = Event::orderBy("id","desc")->take(3)->get();

    	$data['information'] = Information::orderBy("id","desc")->first();

    	$data['resources'] = Resource::orderBy("id","desc")->take(3)->get();

    	return view('user.dashboard',$data);

    }

    public function loadProfile()
    {
        return view("user.profile");
    }

    public function doEditProfile(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
        ]);



//        Auth::user()->first_name = $request->post('first_name');
        $user  = User::where("email",'=',Auth::user()->email)->first();
        $user->first_name = $request->input("first_name");
        $user->last_name = $request->input("last_name");
        $user->about = $request->input("about");
        $user ->save();

        return redirect()->back()->with("message","Profile was successfully updated");

    }
}
