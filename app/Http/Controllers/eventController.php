<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Event;
use DB;


class eventController extends Controller
{
   public function events(){
     return view('events.event');

   }

   public function addevent(Request $request){
       $this->validate($request, [
    		'title'       => 'required',
    		'description' => 'required',
    		'date'        => 'required',
    		'venue'       => 'required'

       ]);

    	$addevent              = new Event;
    	$addevent->title       = $request->input('title');
    	$addevent->description = $request->input('description');
    	$addevent->date        = $request->input('date');
    	$addevent->venue       = $request->input('venue');
    	$addevent->save();
    	return redirect('/event')->with('response', 'Message Successfully Submitted ');
   }


    public function all_events(){

      $events = DB::table('events')->get();

              // dd($contacts);
     return view('events.all_events', [ 'events' => $events]);   
   }

}
