<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Resource;
use Auth;
use DB;




class resourceController extends Controller
{
    public function resource(){

    	return view('resources.resource');
    }



    public function addresource(Request $request){

       $this->validate($request, [
    
    	 	'title'     => 'required',
    	 	'file'      =>'required'
           
    	 	]);



   $video = array('mp4');
   $audio = array('mp3');
   $document = array('docx','pdf','doc');

       	 // return 'validation passed';
       	 $addresource     = new Resource() ;
         $addresource->title = $request->input('title');
         $addresource->file = $request->input('file');
     
        
             $file = Input::file('file');


             $file_extension = $file->getClientOriginalExtension();

             $file_name = time().".".$file->getClientOriginalExtension();

             $file->move(public_path(). '/uploads/', $file_name);

             $url ='/uploads/'.$file_name;
            

             $addresource->file = $url;


             if(in_array($file_extension, $video)){

             	$addresource->file_type = "video";

             }else if(in_array($file_extension, $audio)){
             	$addresource->file_type = "audio";

             }else if(in_array($file_extension, $document)){
             	$addresource->file_type = "document";

             }else{

             	 return redirect('/resource')->with('response', 'Sorry the file you uploaded is unsupported');exit;

             }
		 $addresource->save();
		         
		 return redirect('/resource')->with('response', 'Message Successfully');   

     }




       public function view_resources(){

         $resources = DB::table('resources')->get();

              // dd($contacts);
          return view('resources.view_resources', [ 'resources' => $resources]);   
   }


}
