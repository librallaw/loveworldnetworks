<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Information;
use App\create;
use Auth;
use DB;




class informationController extends Controller
{
    public function create(Request $request){
         return view('information.create');


    }


     public function form(Request $request){

    	 $this->validate($request, [
    	 	'title'   => 'required',
    	 	'message'=> 'required',
           
    	 	]);

       	 // return 'validation passed';
       	 $form = new Information() ;
         $form->title = $request->input('title');
         // $form->user_id = Auth::user()->id;
         $form->description = $request->input('message');
         
         if(Input::hasFile('file')){
             $file = Input::file('file');
             $file->move(public_path(). '/uploads/', $file->getClientOriginalName());
             $url ='/uploads/'. $file->getClientOriginalName();
             // return $url;
             // exit();
             $form->file = $url;
         }
         
         $form->save();
         return redirect('/create')->with('response', 'Message Successfully');

       
    }

    


    public function info_page(){
      $informations = DB::table('informations')->get();
      $informations = Information::orderBy('id','desc')->paginate(4);

              // dd($contacts);
     return view('information.info_page', [ 'informations' => $informations]);   
   }


   public function view_info($id){
        $informations  = Information::where('id', '=', $id)->get();

        	return view('information.view_info', ['informations' => $informations]);

}

    public function moreInfo($id){
        $informations  = Information::where('id', '=', $id)->get();

        	return view('information.moreInfo', ['informations' => $informations]);

}
}
