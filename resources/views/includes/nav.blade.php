
    <header class="side-header expand-header">
        <div class="nav-head">Main Navigation
            <span class="menu-trigger">
                <i class="ion-android-menu"></i>
            </span>
        </div>
        <nav class="custom-scrollbar">
            <ul class="drp-sec">
                <li >
                    <a href="{{route("loaddashboard")}}" title="">
                        <i class="ion-home"></i>
                        <span>Dashboard</span>
                    </a>
                    
                </li> 
                 <li>
                      <a href="{{route("view_resources")}}" title="">
                          <i class="ion-home"></i>
                          <span>Resource Center</span>
                      </a>
                </li>
              
                 <li>
                      <a href="{{route("info_page")}}" title="">
                          <i class="ion-home"></i>
                          <span>Information Desk</span>
                      </a>
                </li>
                  <li>
                     <a href="{{route("all_events")}}" title="">
                          <i class="ion-home"></i>
                          <span>Events</span>
                      </a>
                </li>

            </ul>
            <h4>Admin Section</h4>
             <ul class="drp-sec">
                <li>
                  <a href="{{route("events")}}" title="">
                          <i class="ion-home"></i>
                          <span>Create Resource</span>
                  </a>
                </li>
                <li>
                     <a href="{{route("events")}}" title="">
                          <i class="ion-home"></i>
                          <span>Create Events</span>
                      </a>
                </li>
                 <li>
                      <a href="{{route('create')}}" title="">
                          <i class="ion-home"></i>
                          <span>Create Information</span>
                      </a>
                </li>

            </ul>
            
        </nav>
    </header>