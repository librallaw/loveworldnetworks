<!DOCTYPE html>
<html>

<head>
  <!-- Meta-Information -->
  <title>Profile Page - Loveworld Networks</title>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Vendor: Bootstrap 4 Stylesheets  -->
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

  <!-- Our Website CSS Styles -->
  <link rel="stylesheet" href="css/icons.min.css" type="text/css">
  <link rel="stylesheet" href="css/main.css" type="text/css">
  <link rel="stylesheet" href="css/responsive.css" type="text/css">

  <!-- Color Scheme -->
  <link rel="stylesheet" href="css/color-schemes/color.css" type="text/css" title="color3">
  <link rel="alternate stylesheet" href="css/color-schemes/color1.css" title="color1">
  <link rel="alternate stylesheet" href="css/color-schemes/color2.css" title="color2">
  <link rel="alternate stylesheet" href="css/color-schemes/color4.css" title="color4">
  <link rel="alternate stylesheet" href="css/color-schemes/color5.css" title="color5">
</head>

<body class="expand-data panel-data">
  @include("inc.topbar")
  <!-- Topbar -->

  @include("inc.sidebar")
  <!-- Side Header -->

  <div class="option-panel">
    <span class="panel-btn">
      <i class="fa ion-android-settings fa-spin"></i>
    </span>
    <div class="color-panel">
      <h4>Text Color</h4>
      <span class="color1" onclick="setActiveStyleSheet('color1'); return false;">
        <i></i>
      </span>
      <span class="color2" onclick="setActiveStyleSheet('color2'); return false;">
        <i></i>
      </span>
      <span class="color3" onclick="setActiveStyleSheet('color'); return false;">
        <i></i>
      </span>
      <span class="color4" onclick="setActiveStyleSheet('color4'); return false;">
        <i></i>
      </span>
      <span class="color5" onclick="setActiveStyleSheet('color5'); return false;">
        <i></i>
      </span>
    </div>
  </div>
  <!-- Options Panel -->
  <div class="pg-tp">
    <i class="ion-android-contact"></i>
    <div class="pr-tp-inr">
      <h5>Profile Page</h5>
    </div>
  </div>
  <!-- Page Top -->

  <div class="panel-content">
    <div class="widget pad50-65">
      <div class="profile-wrp">
        <div class="row">


          <div class="col-md-6 col-sm-12 col-lg-6">





            <div class="profile-info-wrp">
              <div class="insta-wrp">
                <span>
                  <img class="brd-rd50" src="images/resource/insta-dp.jpg" alt="" />
                  <span class="sts online"></span>
                </span>
                <div class="insta-inf">
                  <h2>
                    <a href="#" title="">{{Auth::user()->full_name()}}</a>

                  </h2>
                  <span class="desg">{{Auth::user()->network}}

                  </span>

                </div>
              </div>
              <div class="usr-abut">
                <h5 class="prf-edit-tl">About Me

                </h5>
                <p style="max-width:300px;">{{Auth::user()->about}}</p>
              </div>
              <div class="usr-gnrl-inf">
                <h5 class="prf-edit-tl">General Info

                </h5>
                <div class="grn-inf-lst">
                  <i class="fa fa-home"></i>Name:
                  <span class="green-clr">{{Auth::user()->full_name()}}</span>
                </div>
                <div class="grn-inf-lst">
                  <i class="fa fa-envelope"></i> Email:
                  <span>{{Auth::user()->email}}</span>
                </div>
                <div class="grn-inf-lst">
                  <i class="fa fa-building"></i> Network:
                  <span>{{Auth::user()->network}}</span>
                </div>
              </div>


            </div>
          </div>
          <div class="col-md-6 col-sm-12 col-lg-6">
              <div class="profile-info-wrp edit-prf">
                  <h4> Edit Profile Page</h4>

                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif

                @if (session('message'))
                  <div class="alert alert-success">
                    {{ session('message') }}
                  </div>
                @endif

                <form action="" method="post">

                  {{csrf_field()}}

                  <div class="insta-wrp">
                    <div class="insta-inf">
                      <div class="grn-inf-lst">
                        <i class="fa fa-user"></i> First Name:
                        <input type="text" class="brd-rd5" placeholder="" value="{{Auth::user()->first_name}}" name="first_name" required/>
                      </div> <div class="grn-inf-lst">
                        <i class="fa fa-user"></i> Last Name:
                        <input type="text" class="brd-rd5" placeholder="" name="last_name" value="{{Auth::user()->last_name}}" required/>
                      </div>
                    </div>
                  </div>
                  <div class="usr-abut">
                    <h5 class="prf-edit-tl">About Me
                      <i class="fa fa-pencil edit-btn"></i>
                    </h5>
                    <textarea class="brd-rd5" placeholder="Write about yourself..." name="about" >{{Auth::user()->about}}</textarea>
                  </div>

                  <button class="btn btn-success">Update</button>
                </form>
                </div>


              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Panel Content -->
@include("inc.footer")


  <!-- Vendor: Javascripts -->
  <script src="js/jquery.min.js" type="text/javascript"></script>
  <!-- Vendor: Followed by our custom Javascripts -->
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/select2.min.js" type="text/javascript"></script>
  <script src="js/slick.min.js" type="text/javascript"></script>

  <!-- Our Website Javascripts -->
  <script src="js/isotope.min.js" type="text/javascript"></script>
  <script src="js/isotope-int.js" type="text/javascript"></script>
  <script src="js/jquery.counterup.js" type="text/javascript"></script>
  <script src="js/waypoints.min.js" type="text/javascript"></script>
  <script src="js/highcharts.js" type="text/javascript"></script>
  <script src="js/exporting.js" type="text/javascript"></script>
  <script src="js/highcharts-more.js" type="text/javascript"></script>
  <script src="js/moment.min.js" type="text/javascript"></script>
  <script src="js/jquery.circliful.min.js" type="text/javascript"></script>
  <script src="js/fullcalendar.min.js" type="text/javascript"></script>
  <script src="js/jquery.downCount.js" type="text/javascript"></script>
  <script src="js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
  <script src="js/jquery.formtowizard.js" type="text/javascript"></script>
  <script src="js/form-validator.min.js" type="text/javascript"></script>
  <script src="js/form-validator-lang-en.min.js" type="text/javascript"></script>
  <script src="js/cropbox-min.js" type="text/javascript"></script>
  <script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="js/ion.rangeSlider.min.js" type="text/javascript"></script>
  <script src="js/jquery.poptrox.min.js" type="text/javascript"></script>
  <script src="js/styleswitcher.js" type="text/javascript"></script>
  <script src="js/main.js" type="text/javascript"></script>
</body>

</html>