@include('includes/header')
@include('includes/nav')
@include('includes/footer')

<body class="expand-data panel-data">
    <div class="topbar">
        <div class="logo">
            <h1>
                <a href="#" title="">
                   <i class="ion-cube"></i>

                   <h5>LOVEWORLD NETWORKS
                      PORTAL</h5>
                </a>
            </h1>
        </div>
    <div class="topbar-data">
      <div class="usr-act">
        <i>HELLO, JOHN SMITH!</i>
        <span>
          <img src="images/resource/topbar-usr1.jpg" alt="" />
          <i class="sts away"></i>
        </span>
        <div class="usr-inf">
          <div class="usr-thmb brd-rd50">
            <img class="brd-rd50" src="images/resource/usr.jpg" alt="" />
            <i class="sts away"></i>
            <a class="green-bg brd-rd5" href="#" title="">
              <i class="fa fa-envelope"></i>
            </a>
          </div>
          <h5>
            <a href="#" title="">John Smith</a>
          </h5>
          <span>Co Worker</span>
          <i>076 9477 4896</i>
          <div class="act-pst-lk-stm">
            <a class="brd-rd5 green-bg-hover" href="#" title="">
              <i class="ion-heart"></i> Love</a>
            <a class="brd-rd5 blue-bg-hover" href="#" title="">
              <i class="ion-forward"></i> Reply</a>
          </div>
          <div class="usr-ft">
            <a class="btn-danger" href="#" title="">
              <i class="fa fa-sign-out"></i> Logout</a>
          </div>
        </div>
      </div>
      <form class="topbar-search">
        <button type="submit">
          <i class="ion-ios-search-strong"></i>
        </button>
        <input type="text" placeholder="Type and Hit Enter" />
      </form>
      <ul class="tobar-links">
        <li>
          <a href="#" title="">
            <i class="ion-ios-bell"></i>
            <span class="blue-bg">02</span>
          </a>
          <div class="nti-drp-wrp">
            <h5 class="blue-bg">
              <span>You Have</span> 7 Notifications</h5>
            <div class="nti-lst">
              <div class="nti-usr">
                <span class="brd-rd50 rd-bg">
                  <i class="fa fa-cog"></i>
                </span>
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Sadi Orlaf</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>Privacy settings changed</i>
                </div>
              </div>
              <div class="nti-usr">
                <span class="brd-rd50 drkblu-bg">
                  <i class="ion-ios-personadd"></i>
                </span>
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Katti Smith</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>Mike has added you as friend</i>
                </div>
              </div>
              <div class="nti-usr">
                <span class="brd-rd50 orng-bg">
                  <i class="ion-thumbsup"></i>
                </span>
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Willimes Domson</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>like your timeline photo</i>
                </div>
              </div>
              <div class="nti-usr">
                <span class="brd-rd50 grn-bg">
                  <i class="ion-information-circled"></i>
                </span>
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Holli Doe</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>Curabitur id eros limes suscipit blandit.</i>
                </div>
              </div>
            </div>
            <div class="nt-ftr">
              <a href="#" title="">View All</a>
            </div>
          </div>
        </li>
        <li>
          <a href="#" title="">
            <i class="ion-android-drafts"></i>
            <span class="green-bg">10</span>
          </a>
          <div class="nti-drp-wrp">
            <h5 class="green-bg">
              <span>You Have</span> 7 New Messages</h5>
            <div class="nti-lst">
              <div class="nti-usr">
                <img class="brd-rd50" src="images/resource/acti-thmb2.jpg" alt="" />
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Sadi Orlaf</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>Privacy settings changed</i>
                </div>
              </div>
              <div class="nti-usr">
                <img class="brd-rd50" src="images/resource/acti-thmb1.jpg" alt="" />
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Katti Smith</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>Mike has added you as friend</i>
                </div>
              </div>
              <div class="nti-usr">
                <img class="brd-rd50" src="images/resource/acti-thmb3.jpg" alt="" />
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Willimes Domson</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>like your timeline photo</i>
                </div>
              </div>
              <div class="nti-usr">
                <img class="brd-rd50" src="images/resource/acti-thmb4.jpg" alt="" />
                <div class="nti-usr-inr">
                  <h5>
                    <a href="#" title="">Holli Doe</a>
                  </h5>
                  <span class="pst-tm">Just Now</span>
                  <i>Curabitur id eros limes suscipit blandit.</i>
                </div>
              </div>
            </div>
            <div class="nt-ftr">
              <a href="#" title="">View All</a>
            </div>
          </div>
        </li>
        <li>
          <a href="#" title="">
            <i class="ion-android-settings"></i>
          </a>
          <div class="set-lst">
            <div class="set-bd">
              <h4>General Settings</h4>
              <ul class="sett-lst">
                <li>
                  <span class="chck-bx">
                    <input id="set1" type="checkbox">
                    <label for="set1">Report Panel Usage</label>
                  </span>
                  <i>General Settings information</i>
                </li>
                <li>
                  <span class="chck-bx">
                    <input id="set2" type="checkbox">
                    <label for="set2">Mail Redirect</label>
                  </span>
                  <i>General Settings information</i>
                </li>
                <li>
                  <span class="chck-bx">
                    <input id="set3" type="checkbox">
                    <label for="set3">Expose Author Name</label>
                  </span>
                  <i>General Settings information</i>
                </li>
              </ul>
              <h4>Chat Settings</h4>
              <ul>
                <li>
                  <span class="chck-bx">
                    <input id="set4" type="checkbox">
                    <label for="set4">Show Me As Online</label>
                  </span>
                </li>
                <li>
                  <span class="chck-bx">
                    <input id="set5" type="checkbox">
                    <label for="set5">Turn Off Notifications</label>
                  </span>
                </li>
              </ul>
            </div>
            <div class="set-ft">
              <a class="btn-danger" href="#" title="">
                <i class="fa fa-trash"></i> Delete Chat History</a>
            </div>
          </div>
        </li>
        <li>
          <a href="#" title="">
            <i class="ion-android-contacts"></i>
          </a>
          <div class="cnt-lst">
            <ul>
              <li>
                <div class="usr">
                  <img class="brd-rd50" src="images/resource/acti-thmb1.jpg" alt="" />
                  <div class="usr-innr">
                    <h5>
                      <a href="#" title="">Smith Doe</a>
                    </h5>
                    <span>Co Worker</span>
                    <a class="green-bg brd-rd5" href="#" title="">
                      <i class="fa fa-envelope"></i>
                    </a>
                  </div>
                </div>
              </li>
              <li>
                <div class="usr">
                  <img class="brd-rd50" src="images/resource/acti-thmb2.jpg" alt="" />
                  <div class="usr-innr">
                    <h5>
                      <a href="#" title="">Katti Smith</a>
                    </h5>
                    <span>Graphic Designer</span>
                    <a class="green-bg brd-rd5" href="#" title="">
                      <i class="fa fa-envelope"></i>
                    </a>
                  </div>
                </div>
              </li>
              <li>
                <div class="usr">
                  <img class="brd-rd50" src="images/resource/acti-thmb3.jpg" alt="" />
                  <div class="usr-innr">
                    <h5>
                      <a href="#" title="">Willimes Domson</a>
                    </h5>
                    <span>Family Adviser</span>
                    <a class="green-bg brd-rd5" href="#" title="">
                      <i class="fa fa-envelope"></i>
                    </a>
                  </div>
                </div>
              </li>
              <li>
                <div class="usr">
                  <img class="brd-rd50" src="images/resource/acti-thmb4.jpg" alt="" />
                  <div class="usr-innr">
                    <h5>
                      <a href="#" title="">Holli Doe</a>
                    </h5>
                    <span>Company CEO</span>
                    <a class="green-bg brd-rd5" href="#" title="">
                      <i class="fa fa-envelope"></i>
                    </a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
    <div class="topbar-bottom-colors">
      <i style="background-color: #2c3e50;"></i>
      <i style="background-color: #9857b2;"></i>
      <i style="background-color: #2c81ba;"></i>
      <i style="background-color: #5dc12e;"></i>
      <i style="background-color: #feb506;"></i>
      <i style="background-color: #e17c21;"></i>
      <i style="background-color: #bc382a;"></i>
    </div>
  </div>
  <!-- Topbar -->

  <!-- Side Header -->

  <div class="option-panel">
    <span class="panel-btn">
      <i class="fa ion-android-settings fa-spin"></i>
    </span>
    <div class="color-panel">
     <h4 style="color:#2faee0">WELCOME TO LOVEWORLD NETWORKS PORTAL!</h4>
     
    </div>
  </div>
  <!-- Options Panel -->
  <!-- <div class="pg-tp">
    <i class="ion-cube"></i>
    <div class="pr-tp-inr">
      <h4>LoveWorld Networks</h4>
      <span></span>
    </div>
  </div> -->
  <!-- Page Top -->
  <div class="col-md-12 grid-item col-sm-12 col-lg-12">
      <div class="stat-box widget bg-clr3">
          <div class="wdgt-ldr">
          </div>

          <i class="ion-cube"></i>
          <div class="stat-box-innr">
           <div style="text-align:center; font-size:30px; color: #fff">INFORMATION DESK</div>
            <h4></h4>
          </div>
          <span></span>
      </div>
  </div>

  <div class="panel-content">
    <div class="widget pad50-40">
      <div class="timeline-wrp">
        <div class="timeline-innr">
          <div class="timeline-label">
        <!--     <span class="brd-rd5 blue-bg"></span> -->
          </div>




 @if(count($informations)>0)
    @foreach($informations->all() as $information)

   
          <div class="timeline-block">
            <span class="pst-tm">
              <i class="ion-clock"></i> {{$information->created_at}}</span>

   

            <i class="sts away"></i>
            <div class="brd-rd5 act-pst">
              <p><img class="brd-rd" src="{{ URL::to("/").$information->file}}" style="margin: 0 auto;" alt="" /></p><br>
              <div class="act-pst-inf">
                <div class="act-pst-inr">
                  <h5>
                    <a href="#" title="">{{$information->title}}</a><br><br>
                  </h5>
                  <a href="#" title=""></a>
                </div>
                <div class="act-pst-dta">
                  <p>{{$information->description}}</p>
                </div>
              </div>
              <a href="{{route("info_page")}}">
                 <button  class="btn btn-info btn-xs" style="text-align: center;background-color: #009efb;border:none;" >Go Back</button> 
             </a>
            </div>
          </div>
        @endforeach

        @else
            <div><img src="{{ URL::to("/")}}/images/sneeze1.png" style="height: 50px;"></div>
            <p>No Post Available!</p>
     @endif  
      
        </div>

      </div>
    </div>
  </div>
