<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();



Route::group(['middleware'=>['auth']], function (){

    Route::get('/',        'userController@loaddashboard')->name("loaddashboard");
	Route::get('/dashboard',        'userController@loaddashboard')->name("loaddashboard");
	Route::get('/profile',        'userController@loadProfile')->name("loadProfile");
	Route::post('/profile',        'userController@doEditProfile')->name("doEditProfile");
	Route::get('/create',           'informationController@create')->name("create");
	Route::post('/form',            'informationController@form')->name("form");
	Route::get('/info_page',        'informationController@info_page')->name("info_page");
	Route::get('/moreInfo/{id}',    'informationController@moreInfo')->name("moreInfo");
	Route::get('/view_info/{id}',   'informationController@view_info')->name("view_info");
	Route::get('/events',           'eventController@events')->name("events");
	Route::post('/addevent',        'eventController@addevent')->name("addevent");
	Route::get('/all_events',       'eventController@all_events')->name("all_events");
	Route::get('/resource',         'resourceController@resource')->name("resource");
	Route::post('/addresource',     'resourceController@addresource')->name("addresource");
	Route::get('/view_resources',   'resourceController@view_resources')->name("view_resources");
	Route::get('/home', 'HomeController@index')->name('home');

});
